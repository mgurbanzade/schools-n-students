class Student < ApplicationRecord
  has_and_belongs_to_many :class_rooms


  def self.students_tree
    schools = School.includes(class_rooms: :students)

    schools.map do |school|
      [
        school.name,
        school.class_rooms.map do |cls|
          [cls.name, cls.students.pluck(:name)]
        end
      ]
    end
  end

  def self.stats
    ActiveRecord::Base.connection.execute("SELECT * FROM statistics")[0]
  end
end
