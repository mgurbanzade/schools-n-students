class School < ApplicationRecord
  has_many :class_rooms, dependent: :destroy
end
