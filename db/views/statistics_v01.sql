SELECT
  (SELECT COUNT(*) from schools) as schools_count,
  (SELECT COUNT(*) FROM class_rooms) / (SELECT COUNT(*) FROM schools) as avg_class_rooms,
  (SELECT COUNT(*) FROM students) / (SELECT COUNT(*) FROM class_rooms) as avg_students
