class CreateJoinTableStudentsClassRooms < ActiveRecord::Migration[5.2]
  def change
    create_join_table :students, :class_rooms do |t|
      t.index [:student_id, :class_room_id]
      t.index [:class_room_id, :student_id]
    end
  end
end
