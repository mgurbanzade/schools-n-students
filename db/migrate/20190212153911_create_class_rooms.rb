class CreateClassRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :class_rooms do |t|
      t.string :name
      t.integer :school_id

      t.timestamps
    end
  end
end
