3.times do
  School.create(name: Faker::University.name)
end

School.all.each do |s|
  5.times do
    s.class_rooms.create(name: Faker::Educator.subject, school: s)
  end
end

ClassRoom.all.each do |room|
  10.times do
    room.students.create(name: Faker::Name.name)
  end
end

Student.all.each do |student|
  room = student.class_rooms.first
  rooms = room.school.class_rooms

  rooms.where.not(id: room.id).each do |cls|
    student.class_rooms << cls
  end
end
